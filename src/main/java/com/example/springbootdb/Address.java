package com.example.springbootdb;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class Address {
    @Id
    String id;

    @Column
    String name;

    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;

    public Address(String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
    }

    protected Address() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}

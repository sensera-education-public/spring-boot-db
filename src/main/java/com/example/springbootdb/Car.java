package com.example.springbootdb;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
public class Car {
    @Id
    String id;

    @Column
    String name;

    @OneToOne(mappedBy = "car", optional = false)
    private Person person;

    public Car(String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
    }

    protected Car() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}

package com.example.springbootdb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Occupation {
    @Id
    String id;

    @Column
    String name;

    @ManyToMany(mappedBy = "occupations")
    List<Person> persons;

    public Occupation(String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.persons = new ArrayList<>();
    }

    protected Occupation() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }
}

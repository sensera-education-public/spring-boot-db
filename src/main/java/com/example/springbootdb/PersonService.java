package com.example.springbootdb;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
public class PersonService {
    PersonRepository personRepository;
    AddressRepository addressRepository;

    public PersonService(PersonRepository personRepository, AddressRepository addressRepository) {
        this.personRepository = personRepository;
        this.addressRepository = addressRepository;
    }

    public Person createPerson(String name) {
        Person person = new Person(name, 0);
        return personRepository.save(person);
    }

    public Person updatePerson(String personId, String name) {
        Person person = personRepository.getById(personId);
        person.setName(name);
        return personRepository.save(person);
    }

    public Person addAddressToPerson(String personId, String addressAsText) {
        Person person = personRepository.getById(personId);
        Address address = new Address(addressAsText);
        person.addAddress(address);
        addressRepository.save(address);
        return personRepository.save(person);
    }
}

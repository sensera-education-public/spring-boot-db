package com.example.springbootdb;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "person")
public class Person {
    @Id String id;

    @Column(length = 100)
    String name;

    @Column(name = "number_of_cars", nullable = false)
    int numberOfCars;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "car_id", referencedColumnName = "id")
    private Car car;

    @OneToMany(mappedBy = "person")
    List<Address> addresses;

    @ManyToMany
    @JoinTable(
            name = "person_occupation",
            joinColumns = @JoinColumn(name = "persond_id"),
            inverseJoinColumns = @JoinColumn(name = "occupation_id"))
    List<Occupation> occupations;

    public Person(String name, int numberOfCars) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.numberOfCars = numberOfCars;
        this.addresses = new ArrayList<>();
        this.occupations = new ArrayList<>();
    }

    protected Person() {}

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getNumberOfCars() {
        return numberOfCars;
    }
    public void setNumberOfCars(int numberOfCars) {
        this.numberOfCars = numberOfCars;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Occupation> getOccupations() {
        return occupations;
    }

    public void setOccupations(List<Occupation> occupations) {
        this.occupations = occupations;
    }

    public void addAddress(Address address) {
        this.addresses.add(address);
        address.setPerson(this);
    }
}
